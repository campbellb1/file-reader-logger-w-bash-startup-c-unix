cmake_minimum_required(VERSION 3.9)
project(OpSys___Hw2)

set(CMAKE_CXX_STANDARD 11)

add_executable(OpSys___Hw2 main.cpp)