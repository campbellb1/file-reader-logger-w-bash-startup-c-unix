
/*  Programmer: Brandon Campbell
 *  Date:       2/11/2018
 *  Project:    Homework 2
 *  Purpose:    Demonstrates the forking of a process. Child will run Homework 1, while the parent will wait until
 *              the child dies, then will exit.
 */

// Include the C++ i/o library
#include <iostream>
// Include the C-string library
#include <string.h>
// Standard library
#include <stdlib.h>
// Unix library
#include <unistd.h>

#include <wait.h>


// Shortcut for the std namespace
using namespace std;


int main(int argc, char **argv)
{
    pid_t wpid;
    pid_t pid = fork();
    int status = 0;

    if (pid == 0)
    {
        // child process
        cout << "Child's ID: " << getpid() << endl;
        cout << "Parent's ID: " << getpid() << endl;

        execv("/home/brandon/Desktop/TestEnvironment/hw1", argv);
        cout << "hw1 execution failed" << endl;
        exit(0);
    }
    else
    {
        // parent process
        while ((wpid = wait(&status)) > 0); // wait for all child processes to die.
        exit(0);
    }


    return 0;
}